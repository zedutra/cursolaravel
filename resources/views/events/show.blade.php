@extends('layouts.main')

@section('title', $event->nome)

@section('content')

<div class="col-md-10 offset-md-1">
    <div class="row">
        <div id="image-container" class="col-md-6">
            <img src="/img/events/{{ $event->image }}" alt="{{$event->nome}}" class="img-fluid">
        </div>
        <div id="info-container" class="col-md-6">
            <h1>{{ $event->nome }}</h1>
            <p class="event-city"><ion-icon name="location-outline"></ion-icon>{{ $event->cidade }}</p>
            <p class="events-participants"><ion-icon name="people-outline"></ion-icon>99 Participantes</p>
            <p class="event-owner"><ion-icon name="star-outline"></ion-icon>Dono do Evento</p>
            <a href="#" class="btn btn-primary" id="event-submit">Confirmar presença</a>
            <h3>O evento conta com: </h3>
            <ul id="itens-list">
                @foreach($event->itens as $item)
                    <li><ion-icon name="play-outline"></ion-icon><span>{{ $item }}</span></li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-12" id="description-container">
            <h3>Sobre o evento:</h3>
            <p class="event-description">{{ $event->descricao }}</p>
        </div>
    </div>
</div>

@endsection