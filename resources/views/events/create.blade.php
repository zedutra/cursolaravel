@extends('layouts.main')

@section('title', 'Criar Evento')

@section('content')

<div id="event-create-container" class="col-md-6 offset-md3 container">
    <h1>Crie o seu evento</h1>
    <form action="/events" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="image">Imagem do evento:</label>
            <input type="file" class="form-control" id="image" name="image" placeholder="Escolher arquivo">
        </div>
        <div class="form-group">
            <label for="title">Evento:</label>
            <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome do evento">
        </div>
        <div class="form-group">
            <label for="data">Data:</label>
            <input type="date" class="form-control" id="data" name="data">
        </div>
        <div class="form-group">
            <label for="title">Cidade:</label>
            <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Local do evento">
        </div>
        <div class="form-group">
            <label for="title">O evento é privado?</label>
            <select name="privado" id="privado" class="form-control">
                <option value="0">Não</option>
                <option value="1">Sim</option>
            </select>
        </div>
        <div class="form-group">
            <label for="title">Descrição:</label>
            <textarea name="descricao" id="descricao" class="form-control" placeholder="Como será o evento"></textarea>
        </div>
        <div class="form-group">
            <label for="title">Adicione itens de infraestrutura: </label>
            <div class="form-group">
                <input type="checkbox" name="itens[]" value="cadeiras"> Cadeiras
            </div>
            <div class="form-group">
                <input type="checkbox" name="itens[]" value="palco"> Palco
            </div>
            <div class="form-group">
                <input type="checkbox" name="itens[]" value="open bar"> Open bar
            </div>
            <div class="form-group">
                <input type="checkbox" name="itens[]" value="open food"> Open food
            </div>
            <div class="form-group">
                <input type="checkbox" name="itens[]" value="brindes"> Brindes
            </div>
        </div>
        <input type="submit" value="Criar evento" class="btn btn-primary">
    </form>
</div>

@endsection