@extends('layouts.main')

@section('title', 'HDC Events')

@section('content')
<div id="search-container" class="col-md-12">
    <h1>Busque um evento</h1>
    <form action="/" method="GET">
        <input type="text" id="search" name="search" class="form-control" placeholder="Procurar">
    </form>
</div>
<div id="events-container" class="col-md-12">
    @if($search)
        <h2>Buscando por: {{ $search }}</h2>
    @else
        <h2>Próximos eventos</h2>
        <p class="subtitle">Veja os eventos nos próximos dias</p>
    @endif
    <div id="cards-container" class="row">
    @foreach($events as $event)
    <div class="card col-md-3">
        <div class="container-fluid img" style="background-image: url(/img/events/{{ $event->image }});"></div>
        <div class="card-body">
            <div class="card-date">{{ date('d/m/y', strtotime($event->date)) }}</div>
            <h5 class="card-title">{{ $event->nome }}</h5>
            <p class="participants">X participantes</p>
            <a href="/events/{{ $event->id}}" class="btn btn-primary">Saber mais</a>
        </div>
    </div>
    @endforeach
    @if(count($events) == 0 && $search)
        <p>Evento "{{ $search }}" não encontrado. <a href="/">veja outros eventos!</a></p>
    @elseif(count($events) == 0)
        <p class="alert-danger">Não há eventos disponíveis</p>
    @endif
    </div>
</div>

@endsection