<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;

class EventController extends Controller
{
    
    public function index(Request $request){

        $search = request('search');

        if($search){

            $events = Event::where([
                ['nome', 'like', '%'.$search.'%']
            ])->get();
        } else {
            $events = Event::all();
        }

        return view('welcome', ['events' => $events, 'search' => $search]);

    }

    public function create(){

        return view('events.create');

    }

    public function store(Request $request){

        $event = new Event;

        $event->nome = $request->nome;
        $event->cidade = $request->cidade;
        $event->privado = $request->privado;
        $event->descricao = $request->descricao;
        $event->itens = $request->itens;
        $event->date = $request->data;

        // Image Upload
        if($request->hasFile('image') && $request->file('image')->isValid()){

            $requestImage = $request->image;
            
            $extension = $requestImage->extension();
            
            $imageName = md5($requestImage->getClientOriginalName() . strtotime('now')) . $extension;

            $request->image->move(public_path('img/events'), $imageName);

            $event->image = $imageName;

        }

        $user = auth()->user();
        $event->user_id = $user->id;

        $event->save();

        return redirect('/')->with('msg', 'evento criado com sucesso!');

    }

    public function show($id) {

        $event = Event::findOrFail($id);

        return view('events.show', ['event' => $event]);

    }

}
